# Dungeon Bitches
Emily F. Allen:
Game Design, Author and Photography

Sarah Carapace:
Illustrations, Graphics, Calligraphy, Layout, Photography and additional writing contributions

Mxtress Khan:
Copy and Developmental Editing, Layout Assistance, Backer Calligraphy, Kickstarter Manager and Outreach

Play-testing & consultation by Rosalind Chapman, Ania, Holly Smith, Emily Thompson, Lexi Martin, Mxtress Khan, & Sarah Carapace.

# Tetanus Font
Made by Divide By Zero Fonts. See styles/fonts/tetanus/readme.txt for details.