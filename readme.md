# DUNGEON BITCHES
**_A Game In Which Disaster Lesbians Get Fucked Up In Dungeons_**

by **Dying Stylishly Games**

This is an unofficial Foundry VTT implementation of the game Dungeon Bitches by Dying Stylishly Games. It uses the Powered By The Apocalypse (pbta) system. This module only contains the game text - see the file license.md for details.

**About the game**:

https://www.kickstarter.com/projects/dyingstylishlygames/dungeon-bitches-a-queer-ttrpg/description


**Buy the game as as a PDF or book**:
- https://www.drivethrurpg.com/product/375698/Dungeon-Bitches
- https://dungeon-bitches.itch.io/dungeon-bitches

## Game Credits

Emily F. Allen, Sarah Carapace & Mxtress Khan. See the file credits.md for more details.

## License
- This module only uses the game text, which is under a Creative Commons license. See the file license.md for details.
- This module uses the Tetanus Font, made by Divide By Zero Fonts. See styles/fonts/tetanus/readme.txt for details.

The full documentation for this module is available at the Foundry VTT page:
https://foundryvtt.com/packages/dungeon-bitches

