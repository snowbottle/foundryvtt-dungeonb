# Dungeon Bitches
The text of this game is released under a creative commons attribution & share-alike license (CC BY-SA 4.0). You are free to re-use the text here however you wish, provided you provide attribution to the authors and release it under a similar license.

The art, layout and other visual elements remain the property of the creators, and may not be re-used in this way

# Tetanus Font:
See styles/fonts/tetanus/readme.txt for details.